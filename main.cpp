#include <boost/program_options.hpp>
#include <iostream>
#include <fstream>
#include "cmake_config.h"

using namespace std;

#define HELP "help"
#define VERSION "version"
#define DE "german"
#define RU "russian"

int main(int argc, char **argv)
{
    try {
        namespace po = boost::program_options;
        using std::cout;
        using std::cerr;
        using std::endl;
        using std::ifstream;
        typedef std::string str;

        po::options_description generic("Generic options");
        generic.add_options()
            (HELP ",h", "produce help message")
            (VERSION ",v", "print version string")
            (DE ",g", "say hello in german")
            (RU ",r", "say hello in russian")
        ;

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, generic), vm);

        str progname = ((argc > 0) ? argv[0] : "NONAME");

        if (vm.count(HELP)) {
            cout << "Usage: " << progname << " [OPTION]...\n" << generic << endl;
            return 0;
        }

        if (vm.count(VERSION)) {
            cout << progname << " version " PROJECT_VERSION << endl;
            return 0;
        }

        po::notify(vm);

        if (vm.count(DE)) {
            cout << "Hallo, Welt!" << endl;
        }
        else if (vm.count(RU)) {
            cout << "Привет, мир!" << endl;
        }
        else {
            cout << "Hello, world!" << endl;
        }
        return 0;
    }
    catch (const std::exception &e) {
        std::cerr << "Terminating due to exception: " << e.what() << std::endl;
        return 1;
    }
}
