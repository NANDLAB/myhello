cmake_minimum_required(VERSION 3.12)
cmake_policy(SET CMP0048 NEW)
project(myhello VERSION "0.1.3" LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Boost REQUIRED program_options)

set(EXEC "myhello")
set(CONFIG_H "cmake_config.h")

configure_file("${CONFIG_H}.in" "${CONFIG_H}")
add_executable("${EXEC}" main.cpp)
target_link_libraries("${EXEC}" Boost::program_options)

include(GNUInstallDirs)
install(TARGETS "${EXEC}")
