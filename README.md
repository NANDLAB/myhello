# Hello World
A simple Hello World program in C++. It uses [Boost Program Options](https://www.boost.org/doc/libs/release/doc/html/program_options.html).

# Build manually
```
mkdir build
cd build
cmake ..
make
```

# Install from OBS
Binaries for the following distributions are automatically built on [OBS](https://build.opensuse.org/package/show/home:elaunch/myhello):
- Debian_11
- Debian_Testing
- Debian_Unstable
- Raspbian_11
- xUbuntu_20.04
- xUbuntu_20.10
- xUbuntu_21.04
- xUbuntu_21.10

## Add OBS key and repository
If you are not on `Debian_11`, substitute it in the first command with your distribution.
```
DIST=Debian_11
curl "https://build.opensuse.org/projects/home:elaunch/public_key" | gpg --dearmor | sudo tee /usr/share/keyrings/obs-elaunch.gpg > /dev/null
echo "deb [signed-by=/usr/share/keyrings/obs-elaunch.gpg] https://download.opensuse.org/repositories/home:/elaunch/$DIST/ /" | sudo tee /etc/apt/sources.list.d/obs-elaunch.list
```

## Install myhello package
```
sudo apt update
sudo apt install myhello
```
